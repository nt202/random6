(() => {
  function generateRandomString(length) {
    const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let result = "";
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * charset.length);
      result += charset.charAt(randomIndex);
    }
    return result;
  }
  const randomString = generateRandomString(6);
  navigator.clipboard.writeText(randomString).then(function () {
    console.log("random6: " + randomString + " copied!");
  }, function (err) {
    console.error("random6: could not copy text: ", err);
  });
})();
